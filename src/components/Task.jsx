import React from 'react';
import '../sass/task.scss';

function Task(props){
  return(
    <div style={{borderLeft: props.color}} className="task d-flex flex-column my-3 p-2">
      <div className="task-title-container">
        {props.title ? <p className="m-0 p-0">{props.title}</p> : <p className="m-0 p-0">Aucun titre</p>}
      </div>
      {
        props.description !== "" &&
        <div className="task-description-container">
          <p className="m-0 p-0">{props.description}</p>
        </div>
      }
      <div className="task-actions-container">
        <button className="btn">Fait</button>
        <button className="btn">Supprimer</button>
      </div>
    </div>
  )
}

export default Task;
