
import React, {useState, useEffect} from 'react';
import * as _ from 'lodash';
import ConfettiGenerator from "confetti-js";
// import Task from './components/Task';
import './main.scss';
import './sass/task.scss';

function App() {

  const [tasksList, setTasksList] = useState([]);
  const [archivedList, setArchivedList] = useState([]);
  const [categories, setCategories] = useState([]);
  const [filteredTaskList, setFilteredTaskList] = useState([]);

  const [newTaskTitle, setNewTaskTitle] = useState("");
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [newTaskCategory, setNewTaskCategory] = useState("");
  const [newTaskColor, setNewTaskColor] = useState("");

  const [isArchivesVisible, setIsArchivesVisible] = useState(false);

  let defaultTaskColor = "#F56A79";

  useEffect(() => {
    if(localStorage.getItem('tasksList') === null || localStorage.getItem('archived') === null){
      localStorage.setItem('tasksList', JSON.stringify(tasksList));
      localStorage.setItem('archived', JSON.stringify(archivedList));
      localStorage.setItem('categories', JSON.stringify(categories));
    }

    if(localStorage.getItem('tasksList') !== ""){
      setTasksList(JSON.parse(localStorage.getItem('tasksList')));
      setFilteredTaskList(JSON.parse(localStorage.getItem('tasksList')));
    }
    if(localStorage.getItem('archived') !== ""){
      setArchivedList(JSON.parse(localStorage.getItem('archived')));
    }
    if(localStorage.getItem('categories') !== ""){
      setCategories(JSON.parse(localStorage.getItem('categories')));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('tasksList', JSON.stringify(tasksList));
    localStorage.setItem('archived', JSON.stringify(archivedList));
    localStorage.setItem('categories', JSON.stringify(categories));
  }, [tasksList, archivedList, categories]);

  const addTask = (e) => {
    e.preventDefault();

    if(newTaskTitle !== ""){
      let currentList = [...tasksList];
      currentList.push({
        title: newTaskTitle,
        description: newTaskDescription,
        category: newTaskCategory,
        color: newTaskColor
      });
      setTasksList(currentList);
      setCategories([...categories, newTaskCategory]);

      document.getElementById('addTaskForm').reset();
      document.getElementById('taskTitle').style.border = "none";
      document.getElementById('taskTitle').style.color = "#000000";
      document.getElementById('taskTitle').value = "";
    }else{
      document.getElementById('taskTitle').style.border = "1px solid #FF414D";
      document.getElementById('taskTitle').style.color = "#FF414D";
      document.getElementById('taskTitle').value = "Titre nécéssaire";
    }

    setNewTaskTitle("");
    setNewTaskDescription("");
    setNewTaskColor(defaultTaskColor);
    filterTasks('all');
  }

  const removeTask = (list, index) => {
    let currentList, filteredTasksList, currentCatList, filteredCatList;
    switch (list){
      case "task":
      currentList = [...tasksList];
      currentCatList = [...categories];

      delete currentList[index];
      delete currentCatList[index];

      filteredCatList = currentCatList.filter(item => {return item !== undefined});
      filteredTasksList = currentList.filter((item) => {return item !== undefined});
      setTasksList(filteredTasksList);
      setFilteredTaskList(filteredTasksList);
      setCategories(filteredCatList);
      break;
      case "archive":
      currentList = [...archivedList];
      currentCatList = [...categories];

      delete currentList[index];
      delete currentCatList[index];

      filteredCatList = currentCatList.filter(item => {return item !== undefined});
      filteredTasksList = currentList.filter((item) => {return item !== undefined});
      setArchivedList(filteredTasksList);
      setCategories(filteredCatList);
      break;
      default:
      currentList = [...tasksList];
      delete currentList[index];

      filteredTasksList = currentList.filter((item) => {return item !== undefined});
      setTasksList(filteredTasksList);
    }

  }

  const archiveTask = (index) => {
    let currentArchive = [...archivedList];
    let taskToArchive = tasksList[index];
    currentArchive.push(taskToArchive);
    setArchivedList(currentArchive);

    let currentList = [...tasksList];
    delete currentList[index];
    let filteredTasksList = currentList.filter((item) => {return item !== undefined});
    setTasksList(filteredTasksList);
    setFilteredTaskList(filteredTasksList);

    const confettiSettings = { target: 'confetti-canvas', size: '3',  };
    const confetti = new ConfettiGenerator(confettiSettings);
    confetti.render();

    setTimeout(() => {
      confetti.clear();
    }, 1000);
  }


  const filterTasks = (value) => {

    if(value !== "all"){
      setFilteredTaskList(JSON.parse(localStorage.getItem('tasksList')).filter(tasks => tasks.category === value));
    }else{
      setFilteredTaskList(JSON.parse(localStorage.getItem('tasksList')));
    }
  }


  return (
    <div id="app" className="d-flex flex-column align-items-center p-5">
      <p className="infos"><a target="_blank" rel="noreferrer" href="https://enzoavagliano.fr">&copy; Enzo Avagliano</a> - 2021 | Version 1.2.0</p>
      <canvas id="confetti-canvas"></canvas>
      <div className="mb-5 text-center">
        <h1 className="app-title p-0 m-0">todo.enzoavagliano.fr</h1>
        <p className="app-subtitle p-0 m-0">A todo list app</p>
      </div>

      <div className="todo-container d-flex flex-column col-12 col-lg-6">
        <div className="col-4 d-flex flex-column flex-lg-row align-items-lg-center mb-3">
          <p className="m-0 me-3">Afficher </p>
          <form className="d-flex flex-row align-items-center">
            <div>
              <select onChange={(e) => filterTasks(e.target.value)} class="form-select" aria-label="Default select example">
                <option selected value="all">Tout</option>
                {
                  _.uniq(categories).map((cat, index) => {
                    return <option value={cat}>{cat}</option>
                  })
                }
              </select>
            </div>
          </form>
        </div>
        <div className="d-flex flex-column-reverse flex-lg-row justify-content-between">
          <div className={`list col-12 col-lg-7 d-flex flex-column mt-3 mt-lg-0 ${tasksList.length === 0 ? "align-items-center" : ""}`}>
            {
              tasksList.length === 0 ?
              <div className="text-center">
                <p className="p-0 m-0">Aucune tâche à afficher.</p>
                <p className="p-0 m-0">{archivedList.length} tâches archivées</p>
              </div>
              :
              filteredTaskList.map((item, index) => {
                return(
                  <div key={index} style={{borderLeft: `5px solid ${item.color}`}} className="task d-flex flex-column justify-content-between my-3 p-2">
                    <div className="task-title-container mb-2">
                      {item.title ? <p className="task-title m-0">{item.title}</p> : <p className="m-0 p-0">Aucun titre</p>}
                      {item.category ? <small className="task-category m-0">{item.category}</small> : <small>Aucune catégorie</small>}
                    </div>
                    {
                      item.description !== "" &&
                      <div className="task-description-container">
                        <p className="m-0 p-0">{item.description}</p>
                      </div>
                    }
                    <div className="task-actions-container">
                      <button onClick={() => archiveTask(index)} className="btn btn-primary me-2">Archiver</button>
                      <button onClick={() => removeTask('task', index)} className="btn btn-danger">Supprimer</button>
                    </div>
                  </div>
                )
              })
            }
            {
              tasksList.length >= 1 &&
              <div className="text-center">
                <p className="p-0 m-0">{archivedList.length} tâches archivées</p>
              </div>
            }
          </div>
          <div className="actions col-12 col-lg-4">
            <div>
              <form id="addTaskForm" autoComplete="off" onSubmit={addTask}>
                <div className="mb-3 d-flex flex-column">
                  <label htmlFor="taskTitle" className="form-label">Titre de la tâche</label>
                  <input onChange={(e) => setNewTaskTitle(e.target.value)} type="text" className="form-input" id="taskTitle" aria-describedby="emailHelp"/>
                </div>
                <div className="mb-3 d-flex flex-column">
                  <label htmlFor="taskDescription" className="form-label">Description de la tâche <small>(optionnel)</small></label>
                  <input onChange={(e) => setNewTaskDescription(e.target.value)} type="text" className="form-input" id="taskDescription" aria-describedby="emailHelp"/>
                </div>
                <div className="mb-3 d-flex flex-column">
                  <label htmlFor="taskCategory" className="form-label">Catégorie de la tâche <small>(optionnel)</small></label>
                  <input onChange={(e) => setNewTaskCategory(e.target.value)} type="text" className="form-input" id="taskCategory" aria-describedby="emailHelp"/>
                </div>
                <div className="mb-3 d-flex flex-column">
                  <label htmlFor="taskColor" className="form-label">Couleur de la tâche <small>(optionnel)</small></label>
                  <input onChange={(e) => setNewTaskColor(e.target.value)} type="color" defaultValue={defaultTaskColor} className="form-input" id="taskColor" aria-describedby="emailHelp"/>
                </div>
                <button type="submit" className="btn btn-primary">Ajouter la tâche</button>
              </form>
            </div>
            <hr/>
            <div className="d-flex flex-column align-items-center mt-4">
              <div className={`${isArchivesVisible ? "d-flex" : "d-none"} archivedList flex-column col-12`}>
                {
                  archivedList.map((task, index) => {
                    return(
                      <div className="archivedTask ps-2 my-2" key={index}>
                        <p className="p-0 m-0">{task.title} <small>(archivée)</small></p>
                        <button onClick={() => removeTask('archive', index)} className="btn btn-danger">Supprimer définitivement</button>
                      </div>
                    )
                  })
                }
              </div>
              <button onClick={() => setIsArchivesVisible(!isArchivesVisible)} className="btn btn-primary mt-lg-3">{isArchivesVisible ? "Cacher les " : "Voir vos "} tâches archivées ({archivedList.length})</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
