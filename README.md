# Do It! - A todo app

###### Project created by [Enzo Avagliano](https://gitlab.com/EloxFire)
###### Project made with [cra-template](https://create-react-app.dev/docs/getting-started)

___


Create, organize, order your tasks at any moment !


___

###### Version : 1.1.0
###### NPM Version : 14.17.3
###### React Version : 17.0.2
